const express = require('express');
var router = express.Router()

var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

//helpers
var bcrypt = require("../helper/bcrypt")
var jwt=require("../helper/jwt")
var url = process.env.MONGO_URL
var dbname = process.env.MONGO_DB

router.get("/", async (req, res) => {
    res.send("Sigin Up Page")
})
router.post("/", (req, res) => {
    MongoClient.connect(url, { useUnifiedTopology: true }, async function (err, db) {
        if (err) throw err;
        var dbname1 = db.db(dbname);
        await dbname1.collection("users").find({ uemail: req.body.email }).toArray(function (err, result) {
            if (err) throw err;
            if (result.length != 0) {                
                result.forEach(async (data) => {        
                    var pwdcompare = await bcrypt.comparePassword(req.body.pwd, data.upwd)
                    if (pwdcompare) {
                        var token =await jwt.tokenGenerator(data.uemail)
                        res.cookie("user",token)                                                
                      return  res.send("Valid User")
                    } else {
                      return  res.send("In-Valid User")
                    }
                })                
            } else {
              return  res.send("No Data Found")
            }
        });
    })
})
router.get("/login", (req, res) => {
    res.send("login Page")
})
router.post("/login", async (req, res) => {
    var pwdhash = await bcrypt.hashGeneragor(req.body.pwd)
    try {
        MongoClient.connect(url, { useUnifiedTopology: true }, async function (err, db) {
            if (err) throw err;
            var dbname = db.db(dbname);
            var users = await dbname.collection('users').insertOne({
                uname: req.body.name,
                uemail: req.body.email,
                upwd: pwdhash
            }).then(function (result) { return result })
            res.send(users.ops)
        });
    } catch (error) {
        console.log(error)
    }
})

router.get("/logout",async (req,res)=>{
   await res.clearCookie("user")
   return res.send("Cookies Cleared, Check Product Page")
})
module.exports = router