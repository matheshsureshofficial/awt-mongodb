const express = require('express')
var jwt=require("../helper/jwt")
var router = express.Router()

router.get("/",async (req,res)=>{
    const {user} =req.cookies
    var jwtVerify=await jwt.tokenVerify(user)
   if(jwtVerify){
       return res.send("Valid User Continue")
   }else{
       return res.send("Not Valid User")
   }
})
module.exports=router