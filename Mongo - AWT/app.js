const express = require('express')
var dotenv = require('dotenv').config()
var cookieParser=require('cookie-parser')
var app = express()
const port = 4000 || process.env.PORT

//db init
require("./database/mongo")()

//express parser
app.use(express.json())
app.use(express.urlencoded({extended:false}))

//cookie parser
app.use(cookieParser())
//router
const loginpage=require("./routers/login")
const productpage=require("./routers/product")

//router init
app.use("/",loginpage)
app.use("/product",productpage)

app.listen(port, () => { console.log(`App Running On ${port}`) })
