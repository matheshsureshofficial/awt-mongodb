var MongoClient = require('mongodb').MongoClient;
var mongoUrl = process.env.MONGO_URL

// Db Created
module.exports=()=>{
    MongoClient.connect(mongoUrl, { useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        console.log("Database Created Successfully..");
        db.close();
    })
}
