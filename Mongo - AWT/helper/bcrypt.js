const bcrypt = require('bcryptjs')
var saltRounds = 10

module.exports.hashGeneragor = async (pwd) => {
    var salt = await bcrypt.genSalt(saltRounds)
    var pwdhash = await bcrypt.hash(pwd, salt)
    return pwdhash
}

module.exports.comparePassword = async (pwd, hashbwd) => {
    try {
        var result = await bcrypt.compare(pwd, hashbwd)
        return result
    } catch (error) {
        return result = false
    }

}