const jwt = require('jsonwebtoken');

module.exports.tokenGenerator = async (email) => {
    try {
        var token = await jwt.sign(email, process.env.JWT_KEY)        
        return token
    } catch (error) {
        console.log(error)
    }
}

module.exports.tokenVerify=async (token)=>{
    try {
        var data=jwt.verify(token,process.env.JWT_KEY)
        return data
    } catch (error) {
        return false
    }
}